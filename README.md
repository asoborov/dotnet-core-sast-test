# csharp-dotnet-multiproject

Test project with:

* **Language:** C# / .NET Core
* **Package Manager:** Nuget
* **Type:** Multi-projects

## How to use

Please see the [usage documentation](https://gitlab.com/gitlab-org/security-products/tests/common#how-to-use-a-test-project) for Security Products test projects.

## Supported Security Products Features

| Feature             | Supported          |
|---------------------|--------------------|
| SAST                | :white_check_mark: |
| Dependency Scanning | :x: |
| Container Scanning  | :x:                |
| DAST                | :x:                |
| License Management  | :x: |
